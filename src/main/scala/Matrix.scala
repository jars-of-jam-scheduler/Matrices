/**
  * Modelizes a matrix
  * @param content the lines of the matrix
  */
class Matrix(val content : Seq[Seq[Double]] = Seq.empty) {

  /**
    * Computes the product between a point and a matrix
    * @param point the point used in the product
    * @return the new point
    */
  def productPointWithMatrix(point : Seq[Double]) : Seq[Double] = (0 to 3).map(i => (0 to 3).map(i2 => {
    content(i2)(i) * point(i2)
  }).sum)

  /**
    * Computes the transposition of a matrix
    * @return the transposition of a matrix
    */
  def getTheTransposedMatrix: Matrix = {
    val result = Seq.fill(content.head.length)(Seq.fill(content.length)(0))
    new Matrix(result.zipWithIndex.map(couple_ligne_index => couple_ligne_index._1.zipWithIndex.map(couple_column_index => {
      content(couple_column_index._2)(couple_ligne_index._2)
    })))
  }

  /**
    * Computes the product between two matrices
    * @param matrix the second matrix used in the computation
    * @return the product, a matrix
    */
  def productMatrixWithMatrix(matrix : Matrix) : Matrix = {
    if(matrix.content.lengthCompare(content.head.length) != 0) {
      println("The matrix product encountered an error : the matrices are not compatible")
      null

    } else {

      val result = Seq.fill(content.length)(
        Seq.fill(matrix.content.head.length)(0)
      )

      val transposed = matrix.getTheTransposedMatrix

      new Matrix(result.zipWithIndex.map(couple_index_line => couple_index_line._1.zipWithIndex.map(couple_index_coefficient => {
        content(couple_index_line._2).zip(transposed(couple_index_coefficient._2)).map(couple => {
          couple._1 * couple._2
        }).sum
      })))

    }
  }

  /**
    * Gets the identity matrix
    * @param size its size
    * @return the identity matrix
    */
  def getIdentityMatrix(size : Int): scala.collection.mutable.Seq[scala.collection.mutable.Seq[Double]] = {
    scala.collection.mutable.Seq.tabulate(size)(r => scala.collection.mutable.Seq.tabulate(size)(c => if(r == c) 1.0 else 0.0))
  }

  /**
    * This algorithm processes column by column.
    * STEP 1. It finds the greatest coefficient for the current column (called 'a') and, if it equals 0, returns NULL (since the matrix
    * can't be inverted) ; otherwise (STEP 2.), it swaps the pivot's line with this new line and the pivot becomes the adequate coefficient
    * of this new line
    * STEP 3. It divides the pivot's line by the pivot
    * STEP 4. It sets each of the current column's coefficient to 0 by subtracting the corresponding lines by the pivot's line
    * @return
    */
  def getGaussJordanInvertedMatrix: (Matrix, Matrix) = {

    // We get first the matrix to be inverted, second the identity one
    val mutable_being_inversed_matrix : collection.mutable.Seq[collection.mutable.Seq[Double]] = scala.collection.mutable.Seq(content.map(ms => scala.collection.mutable.Seq(ms:_*)):_*)
    val identity_matrix : collection.mutable.Seq[collection.mutable.Seq[Double]] = getIdentityMatrix(content.length)  // We get the identity matrix. It will be modified
    // as the original matrix will.

    content.indices.foreach(general_id_column => {
      println("Current column : " + general_id_column)

      //  STEP 1.
      val id_line_with_max_coefficient_in_this_column = (general_id_column until content.length).maxBy(id_line_in_this_column => Math.abs(mutable_being_inversed_matrix(id_line_in_this_column)(general_id_column)))

      if(mutable_being_inversed_matrix(id_line_with_max_coefficient_in_this_column)(general_id_column) == 0) {
        println("The Gauss-Jordan elimination's algorithm encountered an error : the matrix can't be inverted")

      } else {

        //  STEP 2.
        val tmp_line : scala.collection.mutable.Seq[Double] = mutable_being_inversed_matrix(general_id_column)
        mutable_being_inversed_matrix(general_id_column) = mutable_being_inversed_matrix(id_line_with_max_coefficient_in_this_column)
        mutable_being_inversed_matrix(id_line_with_max_coefficient_in_this_column) = tmp_line

        val identity_tmp_line : scala.collection.mutable.Seq[Double] = identity_matrix(general_id_column)
        identity_matrix(general_id_column) = identity_matrix(id_line_with_max_coefficient_in_this_column)
        identity_matrix(id_line_with_max_coefficient_in_this_column) = identity_tmp_line
        println("\nSWAP DONE")
        println(Console.BLUE + "Original matrix :\n" + Console.RESET + mutable_being_inversed_matrix.mkString("\n"))
        println(Console.RED + "Identity matrix :\n" + Console.RESET + identity_matrix.mkString("\n"))

        //  STEP 3.
        val tmp = mutable_being_inversed_matrix(general_id_column)(general_id_column)
        mutable_being_inversed_matrix(general_id_column) = mutable_being_inversed_matrix(general_id_column).map(coefficient => coefficient / tmp)
        identity_matrix(general_id_column) = identity_matrix(general_id_column).map(coefficient => coefficient / tmp)

        println("\nDIVISION DONE")
        println(Console.BLUE + "Original matrix :\n" + Console.RESET + mutable_being_inversed_matrix.mkString("\n"))
        println(Console.RED + "Identity matrix :\n" + Console.RESET + identity_matrix.mkString("\n"))

        //  STEP 4.
        content.indices.foreach(id_line => {
          val tmp = mutable_being_inversed_matrix(id_line)(general_id_column)

          if(id_line != general_id_column) {
            content.indices.foreach(id_column => {
              mutable_being_inversed_matrix(id_line)(id_column) -= mutable_being_inversed_matrix(general_id_column)(id_column) * tmp
              identity_matrix(id_line)(id_column) -= identity_matrix(general_id_column)(id_column) * tmp
            })
          }

        })

        println("\nSUBTRACTION & MULTIPLICATION DONE")
        println(Console.BLUE + "Original matrix :\n" + Console.RESET + mutable_being_inversed_matrix.mkString("\n"))
        println(Console.RED + "Identity matrix :\n" + Console.RESET + identity_matrix.mkString("\n"))
        println()
      }

    })

    (new Matrix(identity_matrix), new Matrix(mutable_being_inversed_matrix))
  }

  /**
    * Get a line from the matrix
    * @param x the line's ID
    * @return the line N°ID from the matrix
    */
  def apply (x : Int): Seq[Double] = {
    content(x)
  }

  /**
    * String version of the matrix
    * @return the string version of the matrix
    */
  override def toString: String = {
    content.mkString("\n")
  }

}
